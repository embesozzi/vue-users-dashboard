import Vue from 'vue'
import VueMaterial from 'vue-material'
import VueLogger from 'vuejs-logger';

import 'vue-material/dist/vue-material.min.css'
//import 'vue-material/dist/theme/default.css'
//import 'vue-material/dist/theme/black-green-dark.css'
import 'vue-material/dist/theme/default-dark.css'

import App from './App.vue'
import router from "./router";
import store from './store';
import ApiService from "./common/ApiService";



// import VueRouter from 'vue-router'
// import HomeView from '@/views/Home';
// import UsersView from '@/views/Users';
// import RolesView from '@/views/Roles';

/**
const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeView,
    //  redirect: "/users"
      children: [
        {
          path: "/users",
          name: "Users",
          component: UsersView
        },
        {
          path: "/roles",
          name: "Roles",
          component: RolesView
        }
      ]
    }
  ],
});
**/
const isProduction = process.env.NODE_ENV === 'production';
const options = {
  isEnabled: true,
  logLevel : isProduction ? 'error' : 'debug',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
};

ApiService.init();
Vue.config.productionTip = false
Vue.use(VueMaterial)
Vue.use(VueLogger, options);

//Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
