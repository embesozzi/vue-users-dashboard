import ApiService from '@/common/ApiService'

export const UserService = {
    query() {
      return ApiService.get("users");
    },
    get(slug) {
      return ApiService.get("user", slug);
    },
    create(params) {
      return ApiService.post("users", params);
    },
    update(slug, params) {
      return ApiService.update("users", slug, { user: params });
    },
    destroy(slug) {
      return ApiService.delete(`users/${slug}`);
    }
};

//export default UserService;