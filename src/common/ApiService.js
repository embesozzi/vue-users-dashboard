import axios from "axios";
import store from '@/store';

const ApiService = {
  init() {
    //Vue.use(axios);
    axios.defaults.baseURL = process.env.VUE_APP_API_ENDPOINT
    axios.interceptors.response.use(
      response => response,
      handleError
    );
  },
  
  /** 
  setHeader() {
    axios.defaults.headers.common[
      "Authorization"
    ] = 'Token ${JwtService.getToken()}';
  },
  **/
  query() {
    return axios.get("users");
  },

  get(resource, slug = "") {
    return axios.get(`${resource}/${slug}`);
  },

  post(resource, params) {
    return axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return axios.put(`${resource}/${slug}`, params.params);
  },

  put(resource, params) {
    return axios.put(`${resource}`, params);
  },

  delete(resource) {
    return axios.delete(resource);
  }
};

const handleError = error => {
  store.dispatch('populateErrors', `ApiService ${error}`);
};

export default ApiService;