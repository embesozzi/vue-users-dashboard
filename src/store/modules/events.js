const state = {
    errors: [],
    messages: []
  };
  
  const getters = {
    errors: state => state.errors,
    messages: state => state.messages,
  };
  
  const mutations = {
    addError: (state, error) => state.errors.unshift(error),
    popError: state => state.errors.pop(),
    addMessage: (state, message) => state.messages.unshift(message),
    popMessage: state => state.messages.pop(),
  };
  
  const actions = {
    populateErrors: ({ commit }, error) => {
      commit('addError', error);
      setTimeout(() => commit('popError'), 3000);
    },
    sendMessage: ({ commit }, message) => {
        commit('addMessage', message);
        setTimeout(() => commit('popMessage'), 3000);
      }
  };
  
  export default {
    state,
    getters,
    mutations,
    actions,
  };