// import { fetchUsers } from '@/api';
import { UserService } from '@/common/UserService'

const defaultState = {
    users: [],
  };
  
  const actions = {
    getUsers: (context) => {
        UserService.query()
        .then((response) => {
          context.commit('USERS_UPDATED', response.data);
        })
    },
    findUsersByName: (context, params) => {
      UserService.query()
      .then((response) => {
        // API not support query so... filter the content
        var data = response.data;
        if (params) {
          data = data.filter(item => item.username.includes(params))
        }
        context.commit('USERS_UPDATED', data);
        })
      },
      saveUser: (context, params) => {
        UserService.create(params)
        .then(() => {
          context.dispatch('sendMessage', 'Users was created successfully');
          context.dispatch("getUsers");
        })
      }
  
    /**
    async ["getUsers"](context) {
      const { data } = await UserService.query();
      context.commit('USERS_UPDATED', data);
      return data;
    },
    **/
  };
  
  const mutations = {
    USERS_UPDATED: (state, users) => {
      state.users = users;
    },
  };
  
  const getters = {
    users(state) {
      return state.users;
    }
  };
  
  export default {
    state: defaultState,
    getters,
    actions,
    mutations,
  };