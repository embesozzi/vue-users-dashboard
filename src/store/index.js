import Vue from 'vue';
import Vuex from 'vuex';
import usersModule from '@/store/modules/users';
import eventsModule from '@/store/modules/events';


Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    users: usersModule,
    events: eventsModule
  },
});