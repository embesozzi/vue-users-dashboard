import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '@/views/Home';
import UsersView from '@/views/Users';
import UserView from '@/views/User';
import RolesView from '@/views/Roles';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeView,
      redirect: "/users",
      children: [
      {
          path: "/users",
          name: "Users",
          component: UsersView
       },
       {
        path: "/user",
        name: "User",
        component: UserView
        },
        {
          path: "/roles",
          name: "Roles",
          component: RolesView
        }
      ]
    }
  ],
});