# users-dashboard
Vuejs material design users dashboard

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

## Docker

### Install

Build `embesozzi/vue-users-dashboard` from source:
```
docker build -t embesozzi/vue-users-dashboard .
```

### Run the container

Run the image, binding associated ports, and defining your custom variables as environment variables:
```
docker run -d -p 8080:8080 --d --name vue-users-dashboard embesozzi/vue-users-dashboard
```

## Docker Compose

### Run
```
docker-compose up
```

* Add to /etc/hosts

docker-ip   usersdashboard.example.com

* Access to the Vue Dashboard

http://usersdashboard.example.com:8080

### Logs
```
docker-compose logs
```
